import styles from "./ProductCard.module.scss";
import Button from "../button";
import {
  addToCartModalDesc,
  deleteFromCartModalDesc,
} from "../../mock/modalDescriptions";
import StarIcon from "../starIcon";
import PropTypes from "prop-types";
import { useDispatch } from "react-redux";
import { addToCart, deleteFromCart } from "../../redux/actions/cart";
import {
  addToFavorites,
  deleteFromFavorites,
} from "../../redux/actions/favorites";
import { closeModal, openModal } from "../../redux/actions/modal";

export default function ProductCard(props) {
  const dispatch = useDispatch();

  const setIsInFavorite = () => {
    props.isFavorite
      ? dispatch(deleteFromFavorites(props.article))
      : dispatch(addToFavorites(props.article));
  };

  const createModal = (modalData, actionHandlers = []) => {
    const { header, text, closeButton } = modalData;

    const actions = modalData.actions.map((action) => (
      <Button
        backgroundColor={action.backgroundColor}
        text={action.text}
        key={action.key}
        onClick={() => actionHandlers[action.key]}
      />
    ));

    dispatch(openModal({ header, text, closeButton, actions }));
  };

  return (
    <>
      <div className={styles.cardItem}>
        <img className={styles.img} src={props.imgURL} alt="photo" />
        <div className={styles.header}>
          <h2 className={styles.title}>{props.name}</h2>
          <StarIcon
            onClick={setIsInFavorite}
            article={props.article}
            fill={props.isFavorite}
            className={styles.favoriteIcon}
          />
        </div>
        <div className={styles.description}>
          <div className={styles.color}>
            <span className={styles.label}>Color:</span>
            <span className={styles.value}>{props.color}</span>
          </div>
          <div className={styles.article}>
            <span className={styles.label}>Article:</span>
            <span className={styles.value}>{props.article}</span>
          </div>
        </div>
        <div className={styles.priceWrap}>
          <p className={styles.price}>{props.price}$</p>
          {props.inCart ? (
            <Button
              backgroundColor="#0A84FF"
              text="Delete from cart"
              onClick={() =>
                function () {
                  createModal(deleteFromCartModalDesc, [
                    function () {
                      dispatch(closeModal());
                    },
                    function () {
                      dispatch(closeModal());
                      dispatch(deleteFromCart(props.article));
                    },
                  ]);
                }
              }
            />
          ) : (
            <Button
              backgroundColor="#0A84FF"
              text="Add to cart"
              onClick={() =>
                function () {
                  createModal(addToCartModalDesc, [
                    function () {
                      dispatch(closeModal());
                    },
                    function () {
                      dispatch(closeModal());
                      dispatch(addToCart(props.article));
                    },
                  ]);
                }
              }
            />
          )}
        </div>
      </div>
    </>
  );
}

ProductCard.propTypes = {
  name: PropTypes.string,
  price: PropTypes.number,
  imgURL: PropTypes.string,
  article: PropTypes.number,
  color: PropTypes.string,
  isFavorite: PropTypes.bool,
  inCart: PropTypes.bool,
};

ProductCard.defaultProps = {
  isFavorite: false,
  inCart: false,
};
