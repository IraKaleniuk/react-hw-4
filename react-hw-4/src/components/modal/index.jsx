import styles from "./Modal.module.scss";
import CloseBtnIcon from "../closeBtnIcon";
import PropTypes from "prop-types";

export default function Modal(props) {
  return (
    <div className={styles.modal} onClick={props.closeButtonHandler()}>
      <div
        className={
          !props.header.toLowerCase().includes("delete")
            ? styles.window
            : styles.windowDelete
        }
        onClick={(event) => event.stopPropagation()}
      >
        <div className={styles.header}>
          <h3 className={styles.headerTitle}>{props.header}</h3>
          {props.closeButton && (
            <CloseBtnIcon
              onClick={props.closeButtonHandler}
              className={styles.closeBtn}
            />
          )}
        </div>
        <p
          className={
            !props.header.toLowerCase().includes("delete")
              ? styles.text
              : styles.textDelete
          }
        >
          {props.text}
        </p>
        <div
          className={
            !props.header.toLowerCase().includes("delete")
              ? styles.actions
              : styles.actionsDelete
          }
        >
          {props.actions}
        </div>
      </div>
    </div>
  );
}

Modal.propTypes = {
  header: PropTypes.string,
  text: PropTypes.string,
  actions: PropTypes.array,
  closeButton: PropTypes.bool,
  closeButtonHandler: PropTypes.func,
};

Modal.defaultProps = {
  text: "Modal header",
  closeButton: true,
  closeButtonHandler: function () {
    console.log("Close btn handler");
  },
};
