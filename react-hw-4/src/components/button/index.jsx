import styles from "./Button.module.scss";
import PropTypes from "prop-types";

export default function Button(props) {
  return (
    <>
      <button
        className={`${styles.button} ${
          props.text.toLowerCase() === "add to cart"
            ? styles.addToCart
            : props.text.toLowerCase() === "delete from cart"
            ? styles.deleteFromCart
            : ""
        }`}
        style={{ background: props.backgroundColor }}
        onClick={props.onClick()}
      >
        {props.text}
      </button>
    </>
  );
}

Button.propTypes = {
  backgroundColor: PropTypes.string,
  text: PropTypes.string,
  onClick: PropTypes.func,
};

Button.defaultProps = {
  backgroundColor: "#0A84FF",
  text: "Button",
  onClick: function () {
    console.log("Btn clicked!");
  },
};
