import { NavLink } from "react-router-dom";
import styles from "./NoProducts.module.scss";
import PropTypes from "prop-types";

export default function NoProducts(props) {
  return (
    <>
      <h2 className="title">
        You don't have any products in your {props.target} yet
      </h2>
      <NavLink to={"/"} className={styles.link}>
        View all products
      </NavLink>
    </>
  );
}

NoProducts.propTypes = {
  target: PropTypes.string,
};
