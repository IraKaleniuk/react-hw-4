import ProductList from "../components/productList";
import NoProducts from "../components/noProducts";
import { useSelector } from "react-redux";

export function Cart() {
  const { inCart: cart } = useSelector((state) => state.cart);

  return (
    <>
      <main className="main">
        <section className="container">
          {cart.length === 0 ? (
            <NoProducts target="cart" />
          ) : (
            <ProductList filter="cart" />
          )}
        </section>
      </main>
    </>
  );
}
