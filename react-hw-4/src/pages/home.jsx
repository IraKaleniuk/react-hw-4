import ProductList from "../components/productList";
import { useSelector } from "react-redux";
import Preloader from "../components/preloader";

export function Home() {
  let { error, loaded } = useSelector((state) => state.products);

  return (
    <>
      <main className="main">
        <section className="container">
          {error ? (
            <>
              <h2 className="title">Something went wrong! :(</h2>
            </>
          ) : null}
          {!loaded && !error ? <Preloader /> : null}
          {loaded && !error ? (
            <>
              <h2 className="title">Choose your iPhone!)</h2>
              <ProductList />
            </>
          ) : null}
        </section>
      </main>
    </>
  );
}
