import ProductList from "../components/productList";
import NoProducts from "../components/noProducts";
import PropTypes from "prop-types";
import { useSelector } from "react-redux";

export function Favorites() {
  const { inFavorites: favorites } = useSelector((state) => state.favorites);

  return (
    <>
      <main className="main">
        <section className="container">
          {favorites.length === 0 ? (
            <NoProducts target="favorites" />
          ) : (
            <ProductList filter="favorites" />
          )}
        </section>
      </main>
    </>
  );
}

Favorites.propTypes = {
  productList: PropTypes.arrayOf(PropTypes.object),
  onLocalStorageChanged: PropTypes.func,
};
