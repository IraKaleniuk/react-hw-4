export function setItemToLocalStorage(target, data) {
  localStorage.setItem(target, JSON.stringify(data));
}

export function getItemFromLocalStorage(target) {
  return localStorage.getItem(target)
    ? JSON.parse(localStorage.getItem(target))
    : [];
}
