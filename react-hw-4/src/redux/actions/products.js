import { productsTypes } from "../types";

export function fillProducts(data) {
  return {
    type: productsTypes.GET_PRODUCTS_SUCCESS,
    payload: { data },
  };
}
export function getProductsError(message) {
  return {
    type: productsTypes.GET_PRODUCTS_ERROR,
    payload: { message },
  };
}
export function getProductsLoaded() {
  return {
    type: productsTypes.GET_PRODUCTS_REQUESTED,
  };
}

export function getProductsAsync() {
  return async function (dispatch) {
    try {
      const response = await fetch("./products.json");
      const data = await response.json();
      dispatch(fillProducts(data));
      dispatch(getProductsLoaded());
    } catch (error) {
      dispatch(getProductsError(error.message));
    }
  };
}
