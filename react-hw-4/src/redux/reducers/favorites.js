import { favoritesTypes } from "../types";
import {
  getItemFromLocalStorage,
  setItemToLocalStorage,
} from "../../utility/localStorage";

const initialState = {
  inFavorites: getItemFromLocalStorage("productFavorites"),
};

export function favoritesReducer(state = initialState, action) {
  switch (action.type) {
    case favoritesTypes.ADD_TO_FAVORITES: {
      if (!state.inFavorites.includes(action.payload.id)) {
        const newFavorites = [...state.inFavorites, action.payload.id];
        setItemToLocalStorage("productFavorites", newFavorites);
        return {
          ...state,
          inFavorites: [...newFavorites],
        };
      }
      break;
    }
    case favoritesTypes.DELETE_FROM_FAVORITES: {
      if (state.inFavorites.includes(action.payload.id)) {
        const newFavorites = state.inFavorites.filter(
          (product) => product !== action.payload.id
        );
        setItemToLocalStorage("productFavorites", newFavorites);
        return {
          ...state,
          inFavorites: [...newFavorites],
        };
      }
      break;
    }
    default:
      return state;
  }
}
