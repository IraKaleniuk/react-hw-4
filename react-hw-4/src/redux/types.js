export const cartTypes = {
  ADD_TO_CART: "Cart - ADD_TO_CART",
  DELETE_FROM_CART: "Cart - DELETE_FROM_CART",
};

export const favoritesTypes = {
  ADD_TO_FAVORITES: "Favorites - ADD_TO_FAVORITES",
  DELETE_FROM_FAVORITES: "Favorites - DELETE_FROM_FAVORITES",
};

export const productsTypes = {
  GET_PRODUCTS_REQUESTED: "Products - GET_PRODUCTS_REQUESTED",
  GET_PRODUCTS_SUCCESS: "Products - GET_PRODUCTS_SUCCESS",
  GET_PRODUCTS_ERROR: "Products - GET_PRODUCTS_ERROR",
};

export const modalTypes = {
  OPEN_MODAL: "Modal - OPEN_MODAL",
  CLOSE_MODAL: "Modal - CLOSE_MODAL",
};
